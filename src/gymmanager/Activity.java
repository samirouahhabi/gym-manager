package gymmanager;

public class Activity 
{
    protected int _category;
    protected double _fee;
    protected int _billingDay;
    
    public Activity( int c, double f, int bd )
    {
        _category = c;
        _fee = f;
        _billingDay = bd;
    }
    
    public String toString()
    {
        return _category + " - " + _fee + " - " + _billingDay;
    }
}
