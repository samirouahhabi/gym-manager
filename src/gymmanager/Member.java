package gymmanager;

import java.sql.*;
import java.util.*;

public class Member 
{
    public String _firstName;
    public String _lastName;
    public long _dateOfBirth;
    public double _insurance;
    public int _insuranceYear;
    public int _id;
    public String _notes;
    public int _dbId;
    public int _age;
    
    public Member()
    {
    }
    
    public Member( int id )
    {
        _dbId = id;
        load();
    }
    
    public Member( String fn, String ln, long dob, double ins, int insY,
                    int id, String notes )
    {
        _firstName = fn;
        _lastName = ln;
        _dateOfBirth = dob;
        _insurance = ins;
        _insuranceYear = insY;
        _id = id;
        _notes = notes;
        calculateAge();
    }
    
    public String toString()
    {
        String s = _firstName+" "+_lastName;
        
        s += ", "+_age;
        
        return s;
    }
    
    private void calculateAge()
    {
        long curr = new java.util.Date().getTime();
        long age = curr - _dateOfBirth;
        age /= 1000;
        age /= 3600;
        age /= 24;
        age /= 365;
        
        _age = (int)age;
    }
    
    public int save()
    {
        Connection con = null;
        PreparedStatement pst = null;
        int key = -1;

        String url = "jdbc:mysql://"+dbinfo._server+":3306/"+dbinfo._dbase;
        String user = dbinfo._username;
        String password = dbinfo._pw;

        try {
            con = DriverManager.getConnection(url, user, password);

            pst = con.prepareStatement("INSERT INTO members(fname, lname, bday, ins, insY, notes, mId) VALUES(?, ?, ?, ?, ?, ?, ?)");
            pst.setString(1, _firstName);
            pst.setString(2, _lastName);
            pst.setString(3, _dateOfBirth+"");
            pst.setString(4, _insurance+"");
            pst.setString(5, _insuranceYear+"");
            pst.setString(6, _notes);
            pst.setString(7, _id+"");
            pst.executeUpdate();
            
            key = getLastId();
        } 
        catch (SQLException ex) 
        {
            System.err.println(ex);

        } 
        finally 
        {

            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }

            } 
            catch (SQLException ex) 
            {
                System.err.println(ex);
            }
        }
        
        _dbId = key;
        
        return key;
    }
    
    private int getLastId()
    {
        int key = -1;
        try
        {
            String myDriver = "org.gjt.mm.mysql.Driver";
            String myUrl = "jdbc:mysql://"+dbinfo._server+"/"+dbinfo._dbase;
            Class.forName(myDriver);
            Connection conn = DriverManager.getConnection(myUrl, 
                              dbinfo._username, dbinfo._pw);

            String query = "SELECT * FROM members WHERE fname='"+_firstName
                    +"' AND lname='"+_lastName+"' AND bday='"+_dateOfBirth+"'";

            Statement st = conn.createStatement();

            ResultSet rs = st.executeQuery(query);
            
            while (rs.next())
            {
                key = rs.getInt("id");
            }
            st.close();
        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
        }
        
        return key;
    }
    
    private void load()
    {
        try
        {
            String myDriver = "org.gjt.mm.mysql.Driver";
            String myUrl = "jdbc:mysql://"+dbinfo._server+"/"+dbinfo._dbase;
            Class.forName(myDriver);
            Connection conn = DriverManager.getConnection(myUrl, 
                              dbinfo._username, dbinfo._pw);

            String query = "SELECT * FROM members WHERE id='"+_dbId+"'";

            Statement st = conn.createStatement();

            ResultSet rs = st.executeQuery(query);
            
            while (rs.next())
            {
                _firstName = rs.getString("fname");
                _lastName = rs.getString("lname");
                _dateOfBirth = rs.getLong("bday");
                _insurance = rs.getDouble("ins");
                _insuranceYear = rs.getInt("insY");
                _notes = rs.getString("notes");
                _id = rs.getInt("mId");
            }
            st.close();
        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
        }
        
        calculateAge();
    }
}
